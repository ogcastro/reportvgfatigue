\select@language {USenglish}
\contentsline {section}{\numberline {1}Preface}{II}{section.1}
\contentsline {section}{\numberline {2}Blade Design}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Blade Geometry}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Materials}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Composite Layup}{5}{subsection.2.3}
\contentsline {section}{\numberline {3}Simulation set up}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}FAST Setup Parameters}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Polar Modification}{12}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Fatigue Analysis}{13}{subsection.3.3}
\contentsline {section}{\numberline {4}Results and Discussions}{18}{section.4}
\contentsline {section}{\numberline {A}Fortran}{25}{appendix.A}
\contentsline {section}{\numberline {B}Hardware}{26}{appendix.B}
