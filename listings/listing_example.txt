MODULE AeroDyn
!  ....................................................................................
!  AeroDyn
!  ....................................................................................
!  National Wind Technology Center
!  National Renewable Energy Laboratory
!  Golden, Colorado, USA
!
!  Originally created by
!  Windward Engineering, LC
!  ....................................................................................
!  v13.00.00    31 Mar 2010         B. Jonkman           NREL/NWTC
!--------------------------------------------------------------------------------------
   USE                        NWTC_Library
   USE                        SharedTypes

   USE                        InflowWind
   USE                        SharedInflowDefns


   IMPLICIT NONE
   PUBLIC  !BJJ: note that this is a little different than the typical "PRIVATE" unless explicitly
           !stated.  The reason is that now we can just say "USE AeroDyn" and include all the public
           !components of InflowWind, ShareTypes, SharedInflowDefs, etc instead of having to
           !explicitly include those modules, too.  However, care must be taken to explicitly state
           !which variables should be PRIVATE!

   TYPE, PUBLIC :: AeroLoadsOptions