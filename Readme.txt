*******************************************************************************
                                 Smart Blade GmbH 

					  A Latex template for thesis and reports
                              Copyright (C) 2011-2012

                                  Guido Weinzierl
							g.weinzierl(at)smart-blade.com
  
*******************************************************************************

This template shall be used for any kind of report written by or for Smart Blade GmbH

FILES
-----

This templates has several files:
	smbl_latex.tex		:main file.
	header.tex		:includes all settings, like documentclasse, etc.
	titlepage.tex 		:titlepage styles for 1. thesis or 2. report
	nomenclature.tex	:if there are any other abbreviation not made in the text put it here
	chappter files		:content files in seperate folders
		- Preface.tex
		- chapter/...
	appendix/... 		:appendix in sperated files
	listings/...		:put source code .txt files in here to include them
	literatur.bib		:literature file including bib entries

HOW TO USE THIS TEMPLATE
------------------------

1. COPY all the files in a new folder. Rename the 'smbl_latex_template.tex' (e.g. mythesis.tex)
2. If you use TeXnicCenter you can create a new Project with the name of your tex file or use the existing .tcp file
		Be sure to to enable BibTex and MakeIndex.
		Select Menu->Projekt->Eigenschaften and change the name of your .tex file
3. Make your definitions below. You can set your personal data. These definitons are mainly used for the title page.
4. Check the user settings to set the document style. You can choose between:
 	Language: en/de   
	Watermark: on/off (for internal use)
	Document type: thesis/report (changes titlepage and header/footer)
5. List all your chapters in the including section.
6. Write your chapter files. You find examples in the chapterTemplate.tex file.
7. If you want to change the template you will find the documentation for all used packages in the doc folder
8. Have fun with Latex

HOW TO COMPILE
--------------

It might be necessary to install some packages manually. All of them are available from the CTAN server. Check the 
package readme files for more information.
If you use MikTex then check the Settings and enable 'Install missing packages on the fly'. Be sure to work on 
the Admin mode. If a package is installed for only one user there might be some complications.
For the very first run, recompile 3 times!!

You can choose weahter you want to use bibtex or advanced biblatex. Check the user setting 'bibtype'.

After you created the final version of your pdf you can check the PDFTK.dat to update the metainfo of your pdf.

CONTACT
-------

FEEL FREE TO USE AND EDIT THIS TEMPLATE! IF YOU HAVE ANY KIND OF QUESTIONS OR WANT TO GIVE A FEEDBACK CONTACT ME:
g.weinzierl@smart-blade.com

