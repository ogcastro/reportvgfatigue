\select@language {USenglish}
\contentsline {section}{\numberline {1}Preface}{II}{section.1}
\contentsline {section}{\numberline {2}How To}{3}{section.2}
\contentsline {section}{\numberline {3}Section}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Subsection}{3}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Avoid this}{3}{subsubsection.3.1.1}
\contentsline {section}{\numberline {4}Writing text}{3}{section.4}
\contentsline {section}{\numberline {5}Boxes}{3}{section.5}
\contentsline {section}{\numberline {6}Numerations}{3}{section.6}
\contentsline {section}{\numberline {7}Formulas}{4}{section.7}
\contentsline {section}{\numberline {8}Citations}{4}{section.8}
\contentsline {section}{\numberline {9}Nomenclature}{4}{section.9}
\contentsline {section}{\numberline {10}References}{5}{section.10}
\contentsline {section}{\numberline {11}Placing figures and tables}{5}{section.11}
\contentsline {section}{\numberline {12}URLs}{5}{section.12}
\contentsline {section}{\numberline {13}Figures}{5}{section.13}
\contentsline {section}{\numberline {14}Tables}{6}{section.14}
\contentsline {section}{\numberline {15}Include PDF}{8}{section.15}
\contentsline {section}{\numberline {16}Footnotes}{8}{section.16}
\contentsline {section}{\numberline {A}Fortran}{11}{appendix.A}
\contentsline {section}{\numberline {B}Hardware}{12}{appendix.B}
